package com.lineate.traineeship;

public interface EntityService {
    boolean createEntity(User user, String name, String value);

    String getEntityValue(User user, String name);

    boolean updateEntity(User user, String name, String value);
}
