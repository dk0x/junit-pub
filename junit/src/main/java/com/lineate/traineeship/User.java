package com.lineate.traineeship;

import java.util.Collection;

public interface User {
    String getName();

    Collection<Group> getGroups();
}
