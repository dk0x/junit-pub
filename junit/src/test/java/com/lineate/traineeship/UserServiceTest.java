package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class UserServiceTest {
    private ServiceFactory serviceFactory;
    private UserService userService;

    private Collection<Permission> correctPermissions;
    private String correctGroupName;
    private String correctUserName;

    @Before
    public void prepare() {
        serviceFactory = new ServiceFactory();
        userService = serviceFactory.createUserService();
        correctPermissions = Arrays.asList(Permission.read, Permission.write);
        correctGroupName = "group";
        correctUserName = "user";
    }

    @Test
    public void testCreateGroup_withNullOrEmptyOrWhiteSpaceName_mustReturnNull() {
        Assert.assertNull(userService.createGroup(null, correctPermissions));
        Assert.assertNull(userService.createGroup("", correctPermissions));
        Assert.assertNull(userService.createGroup(" ", correctPermissions));
        Assert.assertNull(userService.createGroup("\t", correctPermissions));
    }

    @Test
    public void testCreateGroup_withNullOrEmptyPermissions_mustReturnNull() {
        Assert.assertNull(userService.createGroup(correctGroupName, null));
        Assert.assertNull(userService.createGroup(correctGroupName, new ArrayList<>()));
    }

    @Test
    public void testCreateGroup_withCorrectArguments_mustReturnGroupWithTrimmedName() {
        Group group = userService.createGroup("\t" + correctGroupName + " ", correctPermissions);

        Assert.assertNotNull(group);
        Assert.assertEquals(group.getName(), correctGroupName);
    }

    @Test
    public void testCreateGroup_withCorrectArguments_mustReturnGroupWithEqualsFields() {
        Group group = userService.createGroup(correctGroupName, correctPermissions);

        Assert.assertNotNull(group);
        Assert.assertEquals(group.getName(), correctGroupName);
        Assert.assertEquals(group.getPermissions(), correctPermissions);
    }

    @Test
    public void testCreateGroup_withExistName_mustReturnNull() { // that true?
        Group group = userService.createGroup(correctGroupName, correctPermissions);

        Assert.assertNull(userService.createGroup(correctGroupName, correctPermissions));
    }

    @Test
    public void testGroup_addUserGetUser_mustEquals() {
        Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);
        User correctUser = userService.createUser(correctUserName, correctGroup);

        Assert.assertEquals(correctGroup.getUsers().size(), 0);
        correctGroup.addUser(correctUser);
        Assert.assertTrue(correctGroup.getUsers().contains(correctUser));
        Assert.assertEquals(correctGroup.getUsers().size(), 1);
    }

    @Test
    public void testCreateUser_withNullOrEmptyOrWhiteSpaceName_mustReturnNull() {
        Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);

        Assert.assertNull(userService.createUser(null, correctGroup));
        Assert.assertNull(userService.createUser("", correctGroup));
        Assert.assertNull(userService.createUser(" ", correctGroup));
        Assert.assertNull(userService.createUser("\t", correctGroup));
    }

    @Test
    public void testCreateUser_withNullGroup_mustReturnNull() {
        Assert.assertNull(userService.createUser(correctUserName, null));
    }

    @Test
    public void testCreateUser_withCorrectArguments_mustReturnUserWithEqualsFields() {
        Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);
        User user = userService.createUser(correctUserName, correctGroup);

        Assert.assertNotNull(user);
        Assert.assertEquals(user.getName(), correctUserName);
        Assert.assertTrue(user.getGroups().contains(correctGroup));
    }

    @Test
    public void testCreateUser_withCorrectArguments_mustReturnUserWithTrimmedName() {
        Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);
        User user = userService.createUser(" " + correctUserName + " ", correctGroup);

        Assert.assertNotNull(user);
        Assert.assertEquals(user.getName(), correctUserName);
    }

    @Test
    public void testCreateUser_withExistName_mustReturnNull() {
        Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);
        User user = userService.createUser(" " + correctUserName + " ", correctGroup);

        Assert.assertNull(userService.createUser(" " + correctUserName + " ", correctGroup));
    }

}
