package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

public class EntityServiceTest {
    private ServiceFactory serviceFactory;
    private UserService userService;
    private EntityService entityService;

    private Collection<Permission> correctPermissions;
    private String correctGroupName;
    private Group correctGroup;
    private String correctUserName;
    private User correctUser;

    private String correctEntityName;
    private String correctEntityValue;


    @Before
    public void prepare() {
        serviceFactory = new ServiceFactory();
        entityService = serviceFactory.createEntityService();
        userService = serviceFactory.createUserService();

        correctPermissions = Arrays.asList(Permission.read, Permission.write);
        correctGroupName = "group";
        correctGroup = userService.createGroup(correctGroupName, correctPermissions);
        correctUserName = "user";
        correctUser = userService.createUser(correctUserName, correctGroup);

        correctEntityName = "entity";
        correctEntityValue = "value";

    }

    @Test
    public void testCreateEntity_withNullUser_mustReturnFalse() {
        Assert.assertFalse(entityService.createEntity(null, correctEntityName, correctEntityValue));
    }

    @Test
    public void testCreateEntity_withNullOrEmptyOrWhiteSpaceName_mustReturnFalse() {
        Assert.assertFalse(entityService.createEntity(correctUser, null, correctEntityValue));
        Assert.assertFalse(entityService.createEntity(correctUser, "", correctEntityValue));
        Assert.assertFalse(entityService.createEntity(correctUser, " ", correctEntityValue));
        Assert.assertFalse(entityService.createEntity(correctUser, "\t", correctEntityValue));
    }

    @Test
    public void testCreateEntity_with_mustReturnFalse() {
        Assert.assertFalse(entityService.createEntity(correctUser, null, correctEntityValue));
        Assert.assertFalse(entityService.createEntity(correctUser, "", correctEntityValue));
        Assert.assertFalse(entityService.createEntity(correctUser, " ", correctEntityValue));
        Assert.assertFalse(entityService.createEntity(correctUser, "\t", correctEntityValue));
    }
}
